package itis.socialtest;

import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AnalyticsServiceImpl implements AnalyticsService {
    @Override
    public List<Post> findPostsByDate(List<Post> posts, String date) {
      return   posts.stream().filter(x-> x.getDate().equals(date)).collect(Collectors.toList());
    }

    @Override
    public String findMostPopularAuthorNickname(List<Post> posts) {
      Map<Author, List<Post>> map = posts.stream()
                .collect(Collectors.groupingBy(x-> x.getAuthor()));
      return null;
    }

    @Override
    public Boolean checkPostsThatContainsSearchString(List<Post> posts, String searchString) {
       return posts.stream().anyMatch(x-> x.getContent().contains(searchString));
    }

    @Override
    public List<Post> findAllPostsByAuthorNickname(List<Post> posts, String nick){
       return posts.stream().filter(x -> x.getAuthor().equals(nick)).collect(Collectors.toList());
    }
}
