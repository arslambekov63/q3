package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.*;
import java.util.*;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) throws IOException {
        new MainClass().run("", "");
        MainClass mainClass = new MainClass();
        FileReader fileReader = new FileReader("src\\itis\\socialtest\\resources\\Authors.csv");
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        Map<Long, Author> authors = parseAuthors(bufferedReader);
        FileReader postReader = new FileReader("src\\itis\\socialtest\\resources\\PostDatabase.csv");
        BufferedReader bufferedPostReader = new BufferedReader(postReader);
        List<Post> posts = parsePosts(bufferedPostReader, authors);

        bufferedPostReader.close();
        bufferedReader.close();


        //Задание 1

        posts.stream().forEach(x-> System.out.println(x.getAuthor()+ " " + x.getDate() + " " + x.getLikesCount() + "\n" + x.getContent()));


        //задание 2

        mainClass.analyticsService.findPostsByDate(posts, "17.04.2021").stream()
                .forEach(x-> System.out.println(x.getAuthor()+ " " + x.getDate() + " " + x.getLikesCount() + "\n" + x.getContent()));


        //задание 3

        mainClass.analyticsService.findAllPostsByAuthorNickname(posts, "varlamov").stream().forEach(x-> System.out.println(x.getAuthor()+ " " + x.getDate() + " " + x.getLikesCount() + "\n" + x.getContent()));


        //задание 4


        System.out.println(   mainClass.analyticsService.checkPostsThatContainsSearchString(posts, "Россия"));

    }

    private static Map<Long, Author> parseAuthors(BufferedReader bufferedReader) throws IOException {
        Map<Long, Author> authors = new HashMap<>();
        String str = bufferedReader.readLine();
                while (str!= null) {
            String[] info = str.split(",");
            Author author = new Author(Long.parseLong(info[0]), info[1], info[2]);
            authors.put(author.getId(), author);
            str = bufferedReader.readLine();
        }
        return authors;
    }
    private static List<Post> parsePosts(BufferedReader bufferedReader, Map<Long, Author> authorMap) throws IOException {
        List<Post> posts = new ArrayList<>();
        String str = bufferedReader.readLine();
        while (str!=null) {
            String[] info = str.split(",", 4);
            String[] dataAndTime = info[2].split("T");
            Post post = new Post(dataAndTime[0], info[3], Long.parseLong(info[1]), authorMap.get(info[0]));
            posts.add(post);
            str = bufferedReader.readLine();
        }
        return posts;
    }



    private void run(String postsSourcePath, String authorsSourcePath) {


    }


}
